classes="clerics druids fighters rangers paladins magicusers illusionists thieves assassins monks bards".split(/\s+/);

//classes=new Array('clerics');

demo=document.getElementById("demo");
popin=document.getElementById("popin");
levelmax=document.getElementById("maxlevel");

gobtn.onclick=function()
{
    normals=bounds(popin.value,1,1e10);
    limit=bounds(document.getElementById("maxlevel").value,1,30);
    popin.value=normals;
    levelmax.value=limit;
    npcpop=(normals/2)/100;

    dyn={
        fighters:(npcpop*.4395),
        magicusers:(npcpop/5),
        clerics:(npcpop/5),
        thieves:(npcpop*.1495),
        monks:(npcpop*.01),
        bards:(npcpop*.001),
    }
 //   console.log(npcpop);
    dyn.rangers=(dyn.fighters/10);
    dyn.paladins=(dyn.fighters/10);

    dyn.illusionists=(dyn.magicusers/6);

    dyn.druids=(dyn.clerics/6);

    dyn.assassins=(dyn.thieves/6);

    dyn.fighters=Math.max(dyn.fighters-dyn.paladins-dyn.rangers,0);

    dyn.magicusers=Math.max(dyn.magicusers-dyn.illusionists,0);
    dyn.thieves=Math.max(dyn.thieves-dyn.assassins,0);
    dyn.clerics=Math.max(dyn.clerics-dyn.druids,0);

 //   console.log(dyn);
 //   console.log(classes);
    out="<table width=50% border=1><tr><td></td><th colspan=100>Level</th></tr><tr><th>Class</th>";
    for(i=1;i<=limit;i++)
    {
        out+="<th>"+i+"</th>";
    }
    out+="<th>Total</th></tr>";

    for(const chrclass of classes)
    {
        classpop=dyn[chrclass];
 //       console.log(chrclass);
        if(classpop>0)
        {
            classarrayfn=window["do"+chrclass];
 //           console.log(classarrayfn);
            classarray=classarrayfn(classpop);

            n=classarray.reduce(function(avar,cvar){return avar+cvar},0);
            normals-=n;
            if(n>0)
            {
                row="<tr><td>"+chrclass+"</td>";

 //               console.log(chrclass);
                classarray.shift(); //No level 0
                classarray.forEach((p)=>
                    {
                        if(p)
                        {
                            row=row+"<td align=right>"+p+"</td>";
                        }
                        else
                        {
                            row=row+"<td align=right></td>";
                        }
                    });
                //               console.log(row);
                for(i=classarray.length;i<limit;i++)
                {
                    row=row+"<td align=right>-</td>";
                }
                row+="<td align=right>"+n+"</td>";
                row+="</tr>";
                out+=row;
            }
        }
    };
    out+="<td>0-level</td><td colspan=100 align=right>"+normals+"</td></tr><tr>";
    out+="</table>";
 //   console.log("Normal people: "+normals);

    demo.innerHTML=out;
}

function pick(rate,fixed=0)
{
    if(fixed) return Math.floor(rate);
    return poisson(rate);
}

function poisson(mu)
{
    var p=-mu;
    var n=-1;
    var q=0;
    do
    {
        r=Math.log(Math.random());
        q+=r;
        n++;
    }while(q>p);
    return n;
}

function dolevels(pop,maxlv,xpneeded)
{
//    console.log("levels "+pop);

    perlevel=200;

//    limit=bounds(document.getElementById("maxlevel").value,1,30);

    if(maxlv==0)
    {
        max=limit;
    }
    else
    {
        max=bounds(maxlv,1,limit);
    }

//    console.log(max);

    levels=new Array(0);

    while(xpneeded.length>0)
    {
        n=xpneeded.shift();
        levels.push(n);
    }

    if(levels.length<maxlv)
    {
        perlevel=levels.pop();
    }

//    console.log(levels);

    den=0;
    sum=0;
    prev=0;
    divs=new Array();
    t=0;
    for(let level=1;level <= max; level++)
    {
        if(level<levels.length)
        {
            sum=(levels[level]-prev)/1+prev;
            den+=1/sum;
            prev=levels[level];
        }
        else
        {
            sum=prev+(perlevel/1);
            den+=1/sum;
            prev+=perlevel;
            levels[level]=prev;
        }
        divs[level]=sum;
        t+=sum;
    }

    x=pop/den;
    t=0;
    tl=0;
    c=0;
    result=new Array();

    for(let l=1;l<=max;l++)
    {
        n=pick(x/divs[l]);
        result[l]=n;

        t+=x/divs[l];
        tl+=x/divs[l]*l;
        c+=n;
    }
    //#    print "$c\n";
//    console.log(result);
    return result;
    //    #print "\n$t\n";
    //    #print $tl/$t;
    //    #print "\n";
}

function doclerics(clerics)
{
    return dolevels(clerics,0,new Array(1.5,3,6,13,27.5,55,110,225,225));
}

function dodruids(druids)
{
    return dolevels(druids,23,new Array(2,4,7.5,12.5,20,35,60,90,125,200,300,750,1500,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000));
}

function dofighters(fighters)
{
    return dolevels(fighters,0,new Array(2,4,8,18,35,70,125,250,250));
}

function dorangers(rangers)
{
    return dolevels(rangers,0,new Array(2.25,4.5,10,20,40,90,150,225,325,325));
}

function dopaladins(paladins)
{
    return dolevels(paladins,0,new Array(2.75,5.5,12,24,45,95,175,350,350));
}

function domagicusers(magicusers)
{
    return dolevels(magicusers,0,new Array(2.5,5,10,22.5,40,60,90,135,250,375,375));
}

function doillusionists(illusionists)
{
    return dolevels(illusionists,0,new Array(2.25,4.5,9,18,35,60,95,145,220,220));
}

function dothieves(thieves)
{
    return dolevels(thieves,0,new Array(1.25,2.5,5,10,20,42.5,70,110,160,220,220));
}

function doassassins(assassins)
{
    return dolevels(assassins,15,new Array(1.5,3,6,12,25,50,100,200,300,425,575,750,1000,1500,2000));
}

function domonks(monks)
{
    return dolevels(monks,17, new Array(2.25,4.75,10,22.5,47.5,98,200,350,500,700,950,1250,1750,2250,2750,3250,3750));
}

function dobards(bards)
{
    return dolevels(bards,23,new Array(2,4,8,16,25,40,60,85,110,150,200,400,600,800,1000,1200,1400,1600,1800,2000,2200,3000,4000,500));
}

function bounds(v,mn,mx)
{
    if(v>mx) return mx;
    if(v<mn) return mn;
    return v;
}
