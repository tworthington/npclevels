#!/usr/bin/perl -w

$pop=shift || die("What size of population?\n");
$fixed=shift // 0;
$maxlevel=shift || 23;

$normals=$pop;

$npcpop=($pop/2)/100; # /2 to eliminate children but not old people. Assumes bottom-heavy demographics.
print "$npcpop\n";

$fighters=pick($npcpop*.4395);
$magicusers=pick($npcpop/5);
$clerics=pick($npcpop/5);
$thieves=pick($npcpop*.1495);
$monks=pick($npcpop*.01);
$bards=pick($npcpop*.001);

$rangers=pick($fighters/10);
$paladins=pick($fighters/10);

$illusionists=pick($magicusers/6);

$druids=pick($clerics/6);

$assassins=pick($thieves/6);

$fighters=$fighters-$paladins-$rangers;

$magicusers-=$illusionists;
$thieves-=$assassins;
$clerics-=$druids;

{
    local $,="| \t";
    print "|Level|";
    print (1..$maxlevel);
    print "\n";
    foreach $class (qw(clerics druids fighters rangers paladins magicusers illusionists thieves assassins monks bards))
    {
        if($$class)
        {
            my $fn="do$class";
            @$class=&$fn();
            my $n=sum(@$class);
            $normals-=$n;
            if($n)
            {
                printf '|%s|', ucfirst $class;
                shift @$class;
                print @$class;
                print "\n";
            }
        }
    }
}


print "Normal people:\t $normals\n";

sub poisson
{
    my ($mu)=@_;

    return poissonln($mu) if $mu>100;

    my $p=exp(-$mu);
    my $n=-1;
    my $q=1;
    $|=1;
    do
    {
        $r=rand();
        $q=$q*$r;
        $n++;

    }while($q>$p);
    return $n;
}

sub poissonln
{
    my ($mu)=@_;

    my $p=-$mu;
    my $n=-1;
    my $q=0;
    $|=1;
    do
    {
        $r=log(rand());
        $q=$q+$r;
        $n++;
    }while($q>$p);
    return $n;
}
sub pick
{
    my $rate=shift;
    return roundup($rate) if $fixed ;

    return poisson($rate);
}

sub doclerics
{
    return levels($clerics,0,1.5,3,6,13,27.5,55,110,225,225);
}

sub dodruids
{
    return levels($druids,23,2,4,7.5,12.5,20,35,60,90,125,200,300,750,1500,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000);
}

sub dofighters
{
    return levels($fighters,0,2,4,8,18,35,70,125,250,250);
}

sub dorangers
{
    return levels($rangers,0,2.25,4.5,10,20,40,90,150,225,325,325);
}

sub dopaladins
{
    return levels($paladins,0,2.75,5.5,12,24,45,95,175,350,350);
}

sub domagicusers
{
    return levels($magicusers,0,2.5,5,10,22.5,40,60,90,135,250,375,375);
}

sub doillusionists
{
    return levels($illusionists,0,2.25,4.5,9,18,35,60,95,145,220,220);
}

sub dothieves
{
    return levels($thieves,0,1.25,2.5,5,10,20,42.5,70,110,160,220,220);
}

sub doassassins
{
    return levels($assassins,15,1.5,3,6,12,25,50,100,200,300,425,575,750,1000,1500,2000);
}

sub domonks
{
    return levels($monks,17,2.25,4.75,10,22.5,47.5,98,200,350,500,700,950,1250,1750,2250,2750,3250,3750);
}

sub dobards
{
    return levels($bards,23,2,4,8,16,25,40,60,85,110,150,200,400,600,800,1000,1200,1400,1600,1800,2000,2200,3000,4000,500);
}

sub min
{
    my $n1=shift;
    my $n2=shift;
    return $n1 if $n1<$n2;
    return $n2;
}

sub max
{
    my $n1=shift;
    my $n2=shift;
    return $n1 if $n1>$n2;
    return $n2;
}

sub roundup
{
    my $n=shift;
    return int($n+.5);
}

sub levels
{
    my $pop=shift;

    my $max=shift || $maxlevel;

    my @levels=(0);

    while($n=shift)
    {
        push @levels,$n
    }

    if(@levels<$max)
    {
        $perlevel=pop @levels;
    }
#    print "|$perlevel|$max\n";
    #x = y / (1 / b + 1 / a)

    my $den=0;
    my $sum=0;
    my $prev=0;
    my @divs=();
    my $t=0;
    foreach my $level(1..$max)
    {
        #    print "L $level: $levels[$level]\n";
        if($level<@levels)
        {
            $sum=($levels[$level]-$prev)/1+$prev;
            $den+=1/$sum;
            $prev=$levels[$level];
        }
        else
        {
            #        print "***$prev ";
            $sum=$prev+($perlevel/1);
            $den+=1/$sum;
            $prev+=$perlevel;
            $levels[$level]=$prev;
        }
        #    print "L $level: $levels[$level]\n";
        $divs[$level]=$sum;
        $t+=$sum;
    }

    #print "|$t|$den|\n";
    my $x=$pop/$den;
    #$x/=($den/2) if $den>1;
#    print "$x\n";
    #$x=45;
    $t=0;
    my $tl=0;
    my $c=0;
    my @result=();
    for my $l(1..$max)
    {
#    print "x/$divs[$l]+";
#        printf "Level %02d\t",$l;
        $n=pick($x/$divs[$l]);
        $result[$l]=$n;
#        printf "%f",$n;

        if($n<1)
        {
#            print "\t(1 in ".($divs[$l]/$x).")";
        }
#        print "\n";
        $t+=$x/$divs[$l];
        $tl+=$x/$divs[$l]*$l;
        $c+=$n;
    }
#    print "$c\n";
    return @result;
    #print "\n$t\n";
    #print $tl/$t;
    #print "\n";
}

sub sum
{
    my $n=0;
    my @input=@_;
    foreach my $x(@input)
    {
        $n+=$x if $x;
    }
    return $n;
}
